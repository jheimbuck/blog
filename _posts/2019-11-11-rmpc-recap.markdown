---
layout: post
title:  "Rocky Mountain Product Camp Recap"
date:   2019-11-16
categories: Product Management
---

I was lucky enough to be able to attend Rocky Mountain Product camp again last week and there were some great sessions as always. I wanted to share highlights from the sessions I attended and you can find all the presentations that were shared on this [google sheet](https://docs.google.com/spreadsheets/d/14WNCJVKtMLlNI0pbnky6c4CcpHKDJKTzuvV_4-Ac_SI/edit?usp=sharing).

### Coffee Talk
Before the sessions even started [William](https://www.linkedin.com/in/williamkammersell/) volunteered to lead an impromptu [Lean Coffee](http://agilecoffee.com/leancoffee/) Session for a few of us that were hanging out in the lobby. It was a great discussion about common topics in the industry including How Technical does a PM need to be, how do you help your squad estimate and how to set roadmap expectations with the business.

--

### Your Strategy Needs a Roadmap Too

A great session from Karen M who laid out a definition of a strategic roadmap, talked about why that should not be confused with a release plan and lead an interesting discussion about how much of your roadmap you should share externally. My key takeaway here was that reminder that strategic roadmaps can often and probably should be pitched in the lens of what you're workign on Now, Next and Later without concrete time frames.

--

### Minimum Viable Solution

My friend Matt B lead an interactive session talking about the differences between a Minimal Viable Product and a Minimal Viable Solution. Our group had a user named George who enjoyed roadtrips with his cat that hated the cold. Our MVS was simply a cat carrier with some hot water bottles in it, something we could use to validate some assumptions about how long a cat could be contained, how warm we needed to keep the cat and how much of a pain it would be to take the cat and carrier in and out of the car on the trip.

--

### Decisions, Decisions

Shaughnessy S gave a great talk touching on the topics of Behavioral Economics, Cognitive Bias and how we as Product Managers can use those things positively or negatively, eg; dark patterns. This talk touched on a lot of deep topics and you can quickly go down the rabbit hole of the reading list in the deck for awhile. For me the key take away was how to give choice to a user but nudge them in the way you want. For example the way 401k savings rates jumped up after employers started to make enrollment an opt out vs. opt in for new employees. The employee still has the choice but is most likely to choose to keep the status quo.

--

If you have not attended a Rocky Mountain Product Camp you really should try. The next ones should be in April or May 2020 but you can find information on the [website](https://rmpcamp.org/) or follow the organizers on [Twitter](https://twitter.com/RMPcamp).

