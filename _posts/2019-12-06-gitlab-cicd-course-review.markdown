---
layout: post
title:  "Review of Gitlab CI Udemy Course"
date:   2019-12-06
categories: Gitlab CI/CD
---

I completed a Udemy course covering [Gitlab CI/CD](https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/) and was quite impressed in both the course and the capabilities of GitLab. Full disclosure I am a GitLab employee. I thought I would write up a review of the course to cover things I learned along the way.

### What I learned
Even as a GitLab Product Manager within the [Verify stage](https://about.gitlab.com/direction/cicd/#stages--categories) I was and still am a relative novice at setting up and using CI/CD pipelines so I learned quite a bit. Things that stuck out about the course as interesting to me were:

* Setting up and using [stages](https://docs.gitlab.com/ee/ci/yaml/#stage)
* Setting stages up to run based on branch with [only and except keywords](https://docs.gitlab.com/ee/ci/yaml/#onlyexcept-advanced)
* Launching dynamic [environments](https://docs.gitlab.com/ee/ci/variables/README.html#gitlab-cicd-environment-variables) on [surge.sh](https://surge.sh/)
* Using [caches and artifacts](https://docs.gitlab.com/ee/ci/caching/#cache-vs-artifacts), the differences between them and making use of them to speed up pipeline run times.

### Pros
* I enjoyed the course content and the format of the course quite a bit. It was logically arranged and the video sections were small enough to digest easily.
* The resources provided with each section to GitLab docs or 3rd party resources were excellent.
* I really appreciated the exercises and activities at the end of the course that encouraged you to continue to apply what you learned in a new exercise that did not include the answer. 
* Having other students review that work and provide feedback was a nice surprise as well.

### Cons
* There were several times that changes where made to the gitlab-ci.yml file between sections that I did not cache or things removed that were previously added to demonstrate a concept.
* This was a little confusing to me about what was really required in the file and why failures might be occurring.
* I do wish there would have been an example of publishing to GitLab pages but that was offset with the learnings about dynamic environments and review apps.

### What's Next?
For me this was just an early step in my learning about GitLab CI/CD. I have more Udemy courses queued up about GitLab with one on [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/) up next.
