---
layout: post
title:  "How technical does a Product Manager need to be?"
date:   2020-10-28
categories: Product Management
---

## So should I learn R or Python first?

This question is asked of me quite often, at conferences, at talks, and this week from a candidate asking "How technical are the PMs at company xyz?". Oftentimes the person asking the question wants to know about which new shiny technical they should learn about or if they need to know a programming language or how to train a Machine Learning Model. All of that _**could**_ be helpful, but that's never how I respond to the question.

For a long time my thinking on this was that as a PM you have to be technical enough to get data. This usually means you know some SQL so you can query for data about your product like collecting feature and customer usage. As business intelligence tools have evolved my thinking on this has changed. Tools like omniture, looker, periscope, and of course google analytics amongst hundreds of others have made access to this data, especially for B2C products, very easy to get for analysis. This has made the need to know HOW to get data less critical but still an important tool in the toolbelt for a Product Manager.

Over the last couple years as I have been working with more technical products/services/platforms I have a new angle on this answer. You need to be technical enough to 1) use your product minimally 2) talk about more advanced use cases of your product and 3) empathize with more advanced users of your product.

## Be a day one user

Let's look at an example; Let's say you are a product manager for a platform team, say providing compute to developers like a managed kubernetes platform. So what level of technical expertise do you need? There's a bit to unpack here but let's start with the user, the developer who wants to deploy an application to production. The first thing we need to understand is the workflow, a [user story map](https://www.jpattonassociates.com/user-story-mapping/) would probably be helpful for that.

Once we understand the steps they need to take as a PM we can start to try to replicate this. In this case I'd want to be able to follow along with a simple "[Hello World](https://en.wikipedia.org/wiki/%22Hello,_World!%22_program)" type tutorial, [deploying a simple application](https://cloud.google.com/kubernetes-engine/docs/tutorials/hello-app) into a kubernetes cluster. From there I would talk with users about what would happen after that deployment like what kind of monitoring the team needs for their application and for the compute it's running on, what could go wrong with the application and how they would want know (alerts), how they think they'd fix it (access), etc. As I understand more and more how the user wants to interact with the system I will be able to echo that back to my team. This does not mean i'll be managing my own kubernetes cluster running a distributed application that pages me when it is down but I should understand why a failure to setup auto scaling rules correctly might result in a 2am page to a team when nothing is really wrong<sup>1</sup>.

## You still need to know your metrics!!

I want to revisit my initial lines of this, about understanding the usage of the product. This is still IMPORTANT for any and all Product Managers. In our example kubernetes is very likely replacing "something else" that the team is already using. Even anecdotally understanding how quickly teams are migrating to this solution is crucial so you can gather feedback, plan new features and create a deprecation plan for the old platform.

## Wrap it up dude . . 

So there we go, as with most things in Product Management it comes back to understanding your user and their problem. Once you do that knowing where to get technical and how technical to get will become much more clear. Now back to setting up my own kubernetes cluster . . . 

1 - I made this example up and did not take time to see if I got all that terminology correct, feel free to open an MR to fix it ;)