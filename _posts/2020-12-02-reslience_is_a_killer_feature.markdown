---
layout: post
title:  "Resilience is a killer feature prioritize it accordingly"
date:   2020-12-02
categories: Product management
---

If you tried to start an IoT device, like a smart vacuum, last week from a mobile app you might have seen a message like this.
 
![cannot-connect](http://jheimbuck.gitlab.io/blog/assets/images/cannot-connect-irobot.png)
 
If you, especially my fellow product managers, laughed about this and said to yourself "Of course they should have considered the internet would be down and still make this work come on folks!" then let me remind you that your product has edge cases lurking that will give you a bad or worse day in the future. Thinking about the resilience of your product is not just something we need to leave to development, dev ops, or customer support. Resilience can be a killer feature. If you prioritize it.
 
### Why should you care about resiliency?
 
I get it, you are already super busy talking to customers, writing user stories, crafting an amazing roadmap and presenting it to leadership. Thinking about how to keep a product going when things you do not even have time to understand should n0t be your problem should it? Yes, yes it should. Here are a couple reasons why.
 
1. Down time results in a bad customer experience and you will see this reflected in your KPIs whether they be NPS, engagement or revenue.
1. Trying to get a product back up and running is hard work that is probably going to fall to your development team in part or maybe entirely. Repeated outages have an impact on team morale and will burn folks out. If this is not resolved you will lose good engineers who want to write new features but are constantly fighting outages.
1. If a team is constantly fighting outages they are not thinking about new features no matter how many of them you try to put into an iteration so velocity is going to rightfully decrease. You would not be delivering your best work if you were constantly being pulled away to deal with crises and you cannot expect the development team to either.
 
### How to care about resiliency
 
Hopefully now you WANT to care about the resiliency of your product, but how? Unfortunately you cannot just write into acceptance criteria "The feature/feature is resilient" and it . . . happens. Developers are magical but not that magical. A product manager is in a unique spot to help guide what it means for a product to be resilient though because you have some context about how, where and why a customer is using a feature or product. Let's take the IoT example above with the "smart" vacuum.
 
I ran into the message above myself and after a couple attempts at launching the app I realized all I wanted to do was start the vacuum. I knew I could do this manually and I knew where the vacuum was but it took me an hour to put these things and just go push teh button. So what would have been helpful for users? The Product team for the app probably knows that the majority of time the app is launched it is to start vacuuming (I could be wildly off, i'm assuming) and other functions like setting up a schedule or viewing history are further down the list of jobs a user is trying to do within the app. In this case a link to documentation about how to start the device manually or just a message that the vacuum can be run manually would still let the user do the job they wanted to do. Support of offline history or setting up a schedule that syncs with the cloud later probably are not necessary as there are larger opportunities to be had in building new features but those are areas to explore for the product team.
 
This problem of server availability is easy to spot now that the outage has happened but how could a Product Manager build use cases for resiliency in an app before an outage and without the benefit of a [retrospective](http://jheimbuck.gitlab.io/blog/product/management,/retrospectives/2020/10/12/product-managers-should-attend-incident-reviews.html)? This is where you need to start asking for help.
 
### Who can help with resiliency?
 
Honestly I'm not sure I could tell you without looking it up what Kinesis does and how an outage would impact a feature of mine (if it was part of the architecture) and I don't expect you to either as a product manager. You SHOULD be talking about "downtime" with folks who do know. Some of those people may be:
 
1. Your development team - They were likely involved in deciding the architecture and software/cloud services the application is running on if this is a newer feature or would at least be knowledgeable about what is running if it is an older system. They can tell you about possible failure modes and what it would mean for the product. Probing questions about the liklihood of those failures are a great way to engage the team but do not brush off their fears and assume "it's a major cloud provider they won't go down for long!".
1. The Tech Ops / Dev Ops team (if separate) - This team is going to know the most about limits of hardware/services/software that other teams may have run into. They can tell you about the results of an outage and how it might be prevented in the future or how other services/systems could have similar failure modes.
1. The Support team - If there was an outage they probably got the brunt of the customer complaints about it. Talk to this team about how they supported the outage, what the customer reaction was and what it was preventing them from doing. This might help you design a less ideal experience but one in which a customer is also less impacted or at least in the know about what is happening.
1. The customer - Talk to customers about how other service/product down times impacted them. If your product is a replacement for something existing you should understand how much downtime is "acceptable" and what they need to do during this time. For some products like aviation systems or medical devices NO downtime may be acceptable so understanding the absolute minimal required functionality is critical.
 
So now that we care about resiliency and have talked to lots of different stakeholders about what it means to be resilient we just have to do it right!
 
### Balancing availability and new features
 
Well . . . as Product managers we know it is just not that simple. Building only new features does not work but neither does doing only resilience work. There has to be a balance, and sometimes that balance may be more resilience and less features. This is OK! If building a more resilient product leads to the outcomes customers want it should be prioritized. A user cannot use that shiny new feature if the product is not available can they?
 
Some ways that I have balanced this with development teams (or things I would want to try) are:
 
1. Negotiating a split of items/weight per iteration between features and technical debt/bugs/resiliency. This will and should shift from iteration to iteration. My ask for engineering when we do this is to talk about how we can measure improvements like improving performance or improving test coverage (death to unused code!!!). This can be tricky as you cannot prove a change prevented an incident/outage but you can show a decrease in them over time.
1. Have a dedicated iteration for resilience work. This would come once a quarter and we would focus an entire iteration only on preventative maintenance type work. This did not mean that we would delay critical fixes to once a quarter of course, but things we knew could bite us but had not (and were deemed less likely) would be delayed.
1. Error budgets / SLOs. I have not used these myself but am intrigued by the idea that once a feature has used up its error budget for the month/quarter the features shipped should be focused on resiliency and not new features. This brief explanation maybe slightly/biggly off so I would refer you to [Google's SRE Book](https://sre.google/sre-book/table-of-contents/) for a better reference.
 
### Conclusion
 
So fellow product managers, yes you could just laugh off what happened last week and continue on blissfully ignorant to the ways that your product could fail. OR you could roll up your sleeves, do some work, and build a better product and user experience. You will come out the other side with more knowledge about your product and better relationships with your team. It is worth the work. Good luck!
