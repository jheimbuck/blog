---
layout: post
title:  "Why Product Managers should attend incident reviews"
date:   2020-10-12
categories: Product Management, Retrospectives
---

While listening to a recent [Arrested DevOps](https://www.arresteddevops.com/retropsectives/) one of the panelists mentioned that they think Product should be represented in incident reviews. I agree, and let me tell you why!

1. Availability is a killer feature - Your product or feature cannot meet its goals if no-one can reach it or it is throwing errors. Attend the incident review about why the product failed so you can better understand other places it may fail and how you can write better user stories that account for potential future failure modes.

1. Your software is complicated - Not only is it complex but it relies on many other bits of software and they have their own unique failure modes. Gaining understanding about some of these other dependencies will help you understand how long a feature might take if it relies on upstream or downstream systems. Maybe you can simplify to avoid those dependencies?

1. Noone deserves to be paged for known issues at 2am - For better or worse there will be todos that come out of this gathering. Some of those will end up in your backlog and you get to prioritize them as the Product Manager / Owner. If one of those todos reduces the likelihood that an engineer on your team or an SRE is being woken up at 2am you should prioritize making their life better.

1. Meet new friends! - Even in a small organization you are likely to meet some new people if you attend an incident review who are going to have interesting perspectives about the product. Talk to them, learn some new stuff. It's worth it.

1. Learn new buzzwords - SLI, SLO, SLA, sidekiq, DNS (it's always DNS), cache flush, etc. etc. etc. - Ask what they mean, ask why they are important, expand your knowledge-base.

1. Learning how to learn is cool - There is some really interesting discussion going on in this space and talking about how we can learn from an incident and how it is an investment. Discovering new ways to learn and practice your listening skills is time well spent as a PM.

1. It will make you a better PM - If you go in with an open mind and the intent to learn something new the odds of you wasting 30-60 minutes are almost 0. You can learn of new ways customers are interacting with your feature, reasons the feature is slow and why adding that next new data display might be the tipping point to the whole house of cards falling over.

These are just a few of the reasons to attend an incident review, even if it was not for your service. So if your engineering department is holding incident reviews, post mortem, follow-up call, or some other after downtime review try to make the time to attend.