---
layout: post
title:  "Getting comfortable with things outside your circle"
date:   2020-05-21
categories: Product Management
---

As product managers we are often expected to be masters at being able to influence everything since nothing is within our actual control. The joke in Product circles is something along the lines of "a product maanager is responsible and accountable for everything but in control of nothing". We see this most often when it comes to the delivery teams we work with. We do not manage those engineers but we rely on them to deliver on the product vision that we have laid out, validated with customers and refined with those engineers. There is another post in that about how to get your engineering team aligned and bought into the product vision by having them help craft it but that's another post.

So as a PM we are actually pretty comfortable dealing with things well outside of our realm of control and into the realm of influence, even into just the realm of concern if you subscribe to the Covey model of thought on this topic. And that is probably where we have all struggled the most lately in our day to day, at least I have. I am generally an easy going person and trust that in the end things will work out or at least will not be the worst case scenario. It is either faith in myself, faith in the world or just faith in the fact that I won the genetic lottery as a caucasian male born in the US at just the right time. Regardless I try not to stress myself out and focus on others around me to make sure they are doing ok, feeling confident and able to do their best whether that is my family, friends or co-workers.

This is a different situation though. I have been thinking since March that "things will get back to normal in a few weeks, months at most and we will all get back to our routines".