---
layout: post
title:  "Building a skateboard"
date:   2020-08-01
categories: Product Management
---

![MVP Image](assets/images/mvp.png)

> Image by [Henrik Kniberg](https://blog.crisp.se/wp-content/uploads/2016/01/mvp.png)

As Product Managers we've all seen the diagram above and by now "know" that we should build software iteratively, but why? It's not to make the customer happy right away, I mean look at the person with the skateboard. They are not much happier than the person with only a wheel. Yes they "could" us it to go to work, assuming they are able bodied and the route between their home and work can be navigated by a skateboard. By iteratively building we may realize that there are not good sidewalks for a skateboard OR scooter for our users, but there are bike lanes on the roads. With that feedback in hand we might skip the scooter building step and jump right to a bike.

We quickly see the value of the iterative aproach is in the feedback loop. The creator of the original image has a [great blog about](https://blog.crisp.se/2016/01/25/henrikkniberg/making-sense-of-mvp) this concept that is a far richer explaination than I have given. It is well worth the 15 minutes to read it.

What I really want to talk about is the build time. It often goes unsaid when this diagram is waved around is that it is going to take longer to build the bike than it did to build the skateboard. In fact, the dirty little secret is, if you do end up building the car using an iterative approach it will probably take longer than if you had just started building a car in the first place.

Some people are saying - "Look you just add a handle to it and you have a scooter", but software does not involve just adding a handle. It is about business logic, APIs, data storage, scalability and back office support for projects. Building those supporting things takes time and we know that we cannot scale up a development team as easily as we can new compute instances in the cloud. So as a PM be prepared for geting to a bike, motorcycle and eventually car to take longer.

`example here`

`is this too labored or even the right example?`
An example may help. Imagine you are the product manager for a company that sells coffee online and you want to build a rewards program. Some of the things you may have to figure out and build along the way are; how to track rewards, how to fulfill rewards, how to show a user their rewards status, how to handle support questions about rewards, how to track if rewards users are buying more than non rewards users, etc. To start and test your idea with rapid feedback it may be enough to stand up a static page explaining a program and a signup form, email 25 customers with an offer to join with a link to the page and track and fulfill the orders manually yourself.

In a couple of weeks you should know if you should continue to the next step or not. Now unless you are going to clone yourself this model will not scale up so you have to start building the pieces that will. 

TK - something about building bespoke cars vs. a car factory



