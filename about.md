---
layout: page
title: About
permalink: /about/
---

Hey there, welcome to another blog that I start with good intentions and eventually abandon. I suppose the first iteration of this started in [HyperCard](https://en.wikipedia.org/wiki/HyperCard) and has existed in [GeoCities](https://en.wikipedia.org/wiki/Yahoo!_GeoCities) and [Blogger](https://en.wikipedia.org/wiki/Blogger_(service)) over the years.

This iteration is so I can get some experience with [GitLab](https://www.gitlab.com) and a place to post talk ideas as blogs before they turn into talks.
