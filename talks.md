---
layout: page
title: Talks
permalink: /talks/
---

Some of the conference talks I have given and video of the talk if possible.

----

#### Go to market for Internal Products
* [Slides](https://gitlab.com/jheimbuck/blog/blog/master/_talks/_slides/Go_to_market_with_Internal_Products_-_PLF_Nov_2020.pdf)
* Presented at [Product-Led Festival](https://festival.productledalliance.com/talks/go-to-market-with-internal-products/) Nov 2020

#### DevOps and Product Management Together at last and kicking butt

* [Slides](https://gitlab.com/jheimbuck/blog/blob/master/_talks/_slides/DevOps-and-Product-Management-Together-at-last-May-2019.pdf)
* Presented at [DevOpsDays Denver](https://www.youtube.com/watch?v=8Jh1IjuWzMc) May 2019
* Presented at Velocity San Jose June 2019

#### User Story Mapping

* [Slides](https://gitlab.com/jheimbuck/blog/blob/master/_talks/_slides/Get_Commitment_not_consensus_RMPC_April_2018.pdf)
* Presented with Susan Wilhelm at RMPC April 2019

#### Manging Stakeholders

* [Slides](https://gitlab.com/jheimbuck/blog/blob/master/_talks/_slides/Managing_Stakeholders_RMPC_Nov_2018.pdf)
* Presented with Susan Wilhelm at RMPC November 2018

#### Get commitment not consensus

* [Slides](https://gitlab.com/jheimbuck/blog/blob/master/_talks/_slides/User_Story_Mapping_RMPC_April_2019.pdf)
* Presented at RMPC April 2018

